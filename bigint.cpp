#include "bigint.h"
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <stdlib.h>

Bigint::Bigint()
{
}

Bigint::~Bigint()
{
	free(this->nmb);
}

Bigint::Bigint(unsigned char *nmb, unsigned int alloc_size)
{
	this->nmb = (unsigned char *)realloc(nmb, alloc_size);
	this->alloc_size = alloc_size;
}

Bigint::Bigint(unsigned int bytes)
{
	this->nmb = (unsigned char *) calloc(bytes, sizeof(unsigned char));
	this->alloc_size = bytes;
	if (this->nmb == NULL) {
		throw "Unable to allocate memory for Bigint";
	}
}

void Bigint::setNmb(unsigned char *nmb)
{
	std::memcpy(this->nmb, nmb, this->alloc_size);
}

Bigint *Bigint::copy(Bigint *x)
{
	unsigned char * nmb = (unsigned char *) malloc(x->alloc_size);
	if (nmb == NULL) {
		throw "Unable to allocate memory for Bigint";
	}

	for (int byte = 0; byte < x->alloc_size; byte++) {
		nmb[byte] = x->nmb[byte];
	}

	return new Bigint(nmb, x->alloc_size);
}

Bigint *Bigint::random(int bytes)
{
	unsigned char * nmb = (unsigned char *) malloc(bytes);
	if (nmb == NULL) {
		throw "Unable to allocate memory for Bigint";
	}

	for (int i=0; i<bytes; i++) {
		nmb[i] = rand()%255;
	}

	std::srand(std::time(0));
	return new Bigint(nmb, bytes);
}

void Bigint::reduce()
{
	unsigned int last_nonzero = 0;
	for (int byte=0; byte<this->alloc_size; byte++) {
		if (this->nmb[byte]) {
			last_nonzero = byte;
		}
	}
	last_nonzero++;
	this->nmb = (unsigned char *)realloc(this->nmb, last_nonzero);
	this->alloc_size = last_nonzero;
}


std::string byte2hex(unsigned char x)
{
	std::string charVal = "0123456789ABCDEF";
	char first = charVal[(x&240)>>4];
	char second = charVal[x&15];
	std::string s;
	s += first;
	s += second;
	return s;
}

void Bigint::print()
{
	this->print("");
}

void Bigint::print(const char *s)
{
	std::cout << s;
	for (unsigned char *i = this->nmb; i < this->nmb + this->alloc_size*sizeof(unsigned char); i += sizeof(unsigned char)) {
		std::cout << (int)(*i);
		if (*i) {
			 std::cout << "(0x" << byte2hex(*i) << ")";
		}
		std::cout << " ";
	}
	std::cout << std::endl;
}

/**
 * Use Double dabble to get binary coded decimal for a human readable representation of Bigint
 * 
*/
void Bigint::hrprint(const char *s)
{
	this->reduce();
	//BCD representation needs n+4*ceil(n/3) space
	Bigint *bcd = new Bigint(this->alloc_size+4*(this->alloc_size/3+1));
	
	uint8_t looking_at = 0x80;
	for (int bit=0; bit<this->alloc_size*8; bit++) {
		uint8_t last_byte = this->nmb[this->alloc_size-1];
		uint8_t carry = last_byte&looking_at;
		looking_at >>=1;
		if (looking_at == 0) {
			looking_at = 0x80;
			this->lshift(8);
		}
		for (int i=0; i<bcd->alloc_size; i++) {
			if ((bcd->nmb[i]&0xF) >= 5) {
				bcd->nmb[i] += 3;
			}
			if ((bcd->nmb[i]>>4) >= 5) {
				bcd->nmb[i] += 3<<4;
			}
		}
		bcd->lshift(1);
		if (carry) {
			bcd->add(1);
		}
	}

	bcd->reduce();

	std::cout << s << " ";
	for (int i=bcd->alloc_size-1; i>=0; i--) {
		uint8_t first = (bcd->nmb[i]&0xF0)>>4;
		uint8_t second = bcd->nmb[i]&0xF;
		std::cout << (int)first << (int)second;
	}

	std::cout << std::endl;

	delete bcd;
}

void Bigint::add(unsigned int x)
{
	for (int byte=0; byte<sizeof(x), x>0; byte++) {
		int b = byte;
		uint8_t adding = x&0xFF;
		while (adding) {
			uint16_t result = this->nmb[b];
			result += adding;
			this->nmb[b] = result&0xFF;
			if (result&0x100) {
				adding = 1;
				b++;
			} else {
				break;
			}
		}
		x >>= 8;
	}
}

void Bigint::add(Bigint *x)
{
	for (int byte = 0; byte < x->alloc_size; byte++) {
		int b = byte;
		uint8_t adding = x->nmb[b];
		while (adding) {
			uint16_t result = this->nmb[b];
			result += adding;
			this->nmb[b] = result&0xFF;
			if (result&0x100) {
				adding = 1;
				b++;
			} else {
				break;
			}
		}
	}
}

void Bigint::lshift(const int &amount)
{
	if (amount > 8) {
		throw "Cant shift by more than 1 byte";
	}
	uint8_t add = 0;
	uint8_t looking_at = ~(0xFF>>amount);
	for (int byte = 0; byte < this->alloc_size; byte++) {
		uint8_t overflow = this->nmb[byte]&looking_at;
		this->nmb[byte] <<= amount;
		if (add) {
			this->nmb[byte] += add;
			add = 0;
		}
		if (overflow) {
			//there's no logical shift in C++?!?
			add = overflow>>(8-amount);
		}
	}
}

void Bigint::multiply(Bigint *x)
{
	Bigint *result = new Bigint(this->alloc_size);
	uint8_t unmade_shifts = 0;
	for (int byte = 0; byte < x->alloc_size; byte++) {
		uint8_t multiplying = x->nmb[byte];
		uint8_t i = 0;
		//shifting through big int is costly, do it as little as possible
		while (i++ < 8) {
			if (multiplying&1) {
				if (unmade_shifts) {
					this->lshift(unmade_shifts);
					unmade_shifts = 0;
				}
				result->add(this);
			}
			unmade_shifts++;
			if (unmade_shifts >= 8) {
				this->lshift(8);
				unmade_shifts -= 8;
			}
			multiplying >>= 1;
		}
	}
	this->setNmb(result->nmb);
	delete result;
}
