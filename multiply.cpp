#include <iostream>

#include "bigint.h"

int main()
{
	Bigint *a = Bigint::random(3200);
	Bigint *b = Bigint::random(3200);
	a->print("a: ");
	b->print("b: ");
	Bigint *c = new Bigint(a->nmb, 6400);
	c->print("c: ");
	c->multiply(b);
	c->hrprint("atsakymas");

	delete a;
	delete b;
	delete c;
	return 0;
}