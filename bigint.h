#include <iostream>
//no reason not to use an already existing Bigint lib other than fun
#include <vector>

class Bigint
{
public:
	Bigint();
	Bigint(unsigned char *nmb, unsigned int alloc_size);
	Bigint(unsigned int bytes);
	~Bigint();
	void setNmb(unsigned char *nmb);
	static Bigint *random(int symbols);
	static Bigint *copy(Bigint *x);
	void reduce();
	void print();
	void print(const char *s);
	void hrprint(const char *s);
	void add(unsigned int x);
	void add(Bigint *x);
	void multiply(Bigint *x);
	void lshift(const int &amount);
	unsigned char *nmb;
	unsigned int alloc_size;
};